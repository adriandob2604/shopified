import React from 'react'
import ReactDOM from 'react-dom/client'
import Navbar from './components/homepage/navbar.tsx'

import './index.css'

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <Navbar/>

  </React.StrictMode>
)
