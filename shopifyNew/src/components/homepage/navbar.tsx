import shopifyLogo from 'C:/Users/adria/shopified/shopifyNew/src/assets/shopifyLogo.jpg'
import bell from 'C:/Users/adria/shopified/shopifyNew/src/assets/bell.gif'
import Refresh from './allButtons'
function Navbar(): JSX.Element {
    const handleRefresh = (): void => {
        Refresh()
    }
    return (
        <>
            <nav>
                <img src={shopifyLogo} width={256} height={128} alt="shopify" onClick={handleRefresh}></img>
                <div className='search-bar'>
                        <input className="search"/>
                        <div>CTRL</div>
                        <div>K</div>
                </div>
                <img className='notification' src={bell} width={256} height={128}></img>
            </nav>
        </>
    )
}
export default Navbar
